/*
////////////////////////////////////////////////////////////////////////
                WiFi SWITCH
////////////////////////////////////////////////////////////////////////
 Version: Web Server WiFi Switching Version 1.05
 Autor:  Raul E. Aguirre H. - Chihuahua,Mexico

 Revisiones
 1. Invertir las salidas. - Hecho en V0.09
 2. Posibilidad de cambiar el nombre de los botones - Hecho en V0.09
 3. Proteger con contraseña. - Hecho en V0.09
 4. Actualizar página configurable. - Hecho en V0.09
 5. Encender o apagar las salidas en el arranque. - Hecho en V0.09
 6. Activar / desactivar el Todo en los botones de encendido / apagado. - Hecho en V1.02
 7. Leer la temperatura. - Hecho en V0.09
 8. Guardar / Cargar Estados en EEPROM para mantener último estado después del corte de energía eléctrica. - Hecho en V1.02
 9. Opción de elegir salida para retener valores después de un corte de energía. - Hecho en V0.09
 10. Agregada compatibilidad Temperatura Analoga.- Hecho en V1.02.1
 11. Agregado Acceso Directo a Servidor.- Hecho en V1.02.1
 12. Agregado AutoconexionAP.- Hecho en V1.03
 13. Agregado Usuario y Contraseña.- Hecho en V1.04.1
 14. Agregado Servicio MQTT y Static IP.- Hecho en V1.05
 15. Agregado FS y Json.- Hecho en V1.05

ESP8266 12F
*/
#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//nesesario para library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager

#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson
#include <SPI.h>
#include <EEPROM.h>
#include <WiFiClient.h>
#include <String.h> // Usado para Logear
//define sus Valores default, si son diferentes a los valores en config.json, se sobreescriben.
//longitud maxima de + 1 
char mqtt_server[40];
char mqtt_port[6] = "8080";
char blynk_token[33] = "SU_TOKEN_REDCORP";
//default custom static IP
char static_ip[16] = "192.168.1.200";
char static_gw[16] = "192.168.1.1";
char static_sn[16] = "255.255.255.0";

//bandera para guardar los datos
bool shouldSaveConfig = false;
WiFiServer server = WiFiServer(80);
//El número de salidas que cambiaran..
int outputQuantity = 4;  //No debe excederse de 10 

//Invertir la salida de los LED
boolean outputInverted = false; //true or false
// Esto se hace en caso de que la placa de relé dispare el relé en negativo, en lugar de alimentación positiva

//actualización de página HTML 
int refreshPage = 60; //por defecto es de 60 seg. 
//Tenga en cuenta que si lo hace volver a cargar demasiado rápido, la página podría llegar a ser inaccesibles.

//Mostrar u ocultar el "Interruptor de todos los botones" en la parte inferior de la página
int switchOnAllPinsButton = false; //true or false

//grupo de teclas
//las variables empiezan desde 0 a 9, 0 se cuenta, así, de este modo obtenemos las 10 salidas.

// Seleccione la Direccion del pin de salida
int outputAddress[4] = {2,3,4,5}; //Asignar hasta 10 espacios y el nombre de la dirección del pin de salida.

// Escribir el texto de la descripción del canal de salida
String buttonText[4] = {"01. Luces","02. Cochera","03. Persianas","04. Automatico"};

// Ajuste la salida para retener el último estado después de corte de energia.
int retainOutputStatus[4] = {1,1,1,1};//1-retener el último estado. 0-será apagado después de corte de energía.

////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//DECLARACION DE VARIABLES
////////////////////////////////////////////////////////////////////////
int outp = 0;
boolean printLastCommandOnce = false;
boolean printButtonMenuOnce = false;
boolean initialPrint = true;
String allOn = "";
String allOff = "";
boolean reading = false;
boolean outputStatus[4]; //Crear una matriz booleana para la cantidad máxima.
String rev = "V1.05";
unsigned long timeConnectedAt;
boolean writeToEeprom = false;
String readString; //logeo
boolean login=false; //logeo
//
/////////////////////////////////////////////////
// lectura relacionada a Temperatura
const int tempInPin = 17;
double tempInValue = 0.0; // leer temperatura
double celsius = 0.0;
//Devolucion de LLamada, notifica la necesidad de guardar configuracion
void saveConfigCallback () {
  Serial.println("Debe Guardar Configuracion");
  shouldSaveConfig = true;
}

void setup() {
  
  Serial.begin(115200);
  Serial.println();

  //FS Limpias, para las Pruebas
  //SPIFFS.format();

  //Lee Configuracion de FS json
  Serial.println("Montando FS...");

  if (SPIFFS.begin()) {
    Serial.println("Montando Sistema de archivos");
    if (SPIFFS.exists("/config.json")) {
      //El Archivo Existe, Leyendo y Cargando
      Serial.println("Leyendo Archivo de Configuracion");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("Abriendo Archivo de Configuracion");
        size_t size = configFile.size();
        // Asignar búfer para almacenar contenido del archivo.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonBuffer jsonBuffer;
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        json.printTo(Serial);
        if (json.success()) {
          Serial.println("\nAnalizando json");

          strcpy(mqtt_server, json["mqtt_server"]);
          strcpy(mqtt_port, json["mqtt_port"]);
          strcpy(blynk_token, json["blynk_token"]);

          if(json["ip"]) {
            Serial.println("Configurar IP Personalizado Desde config");
            //static_ip = json["ip"];
            strcpy(static_ip, json["ip"]);
            strcpy(static_gw, json["gateway"]);
            strcpy(static_sn, json["subnet"]);
            //strcat(static_ip, json["ip"]);
            //static_gw = json["gateway"];
            //static_sn = json["subnet"];
            Serial.println(static_ip);
/*            Serial.println("converting ip");
            IPAddress ip = ipFromCharArray(static_ip);
            Serial.println(ip);*/
          } else {
            Serial.println("IP no Personalizada en config");
          }
        } else {
          Serial.println("Fallo al Cargar Configuracion de json");
        }
      }
    }
  } else {
    Serial.println("Fallo al Montar Sistema de Archivos");
  }
  //Fin de Lectura
  Serial.println(static_ip);
  Serial.println(blynk_token);
  Serial.println(mqtt_server);


  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 5);
  WiFiManagerParameter custom_blynk_token("blynk", "blynk token", blynk_token, 34);

pinMode(tempInPin,INPUT);
  initEepromValues();
  readEepromValues();
   
  //Setear pines como salidas 
  boolean currentState = false;
  for (int var = 0; var < outputQuantity; var++){

    pinMode(outputAddress[var], OUTPUT);       

    //Conmutar todas las salidas a encendido o apagado durante el inicio
    if(outputInverted == true) {
      //
      if(outputStatus[var] == 0){currentState = true;}else{currentState = false;} //comprobar el outputStatus Si está desactivado, cambiar la salida en consecuencia
      digitalWrite(outputAddress[var], currentState);
      
    }
    else{
      
      if(outputStatus[var] == 0){currentState = false;}else{currentState = true;}//comprobar el outputStatus Si está desactivado, cambiar la salida en consecuencia
      digitalWrite(outputAddress[var], currentState);
    }

  }
  //WiFiManager
  //inicialización local. Una vez que se lleva a cabo su actividad, no hay necesidad de mantenerlo en torno
  WiFiManager wifiManager;

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  //set static ip
  IPAddress _ip,_gw,_sn;
  _ip.fromString(static_ip);
  _gw.fromString(static_gw);
  _sn.fromString(static_sn);

  wifiManager.setSTAStaticIPConfig(_ip, _gw, _sn);
  
  //añadir todos sus parámetros aquí
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_blynk_token);

  //reset settings - for testing
  //wifiManager.resetSettings();

  //establecer la calidad mínima de la señal por lo que no tiene en cuenta los AP en virtud de calidad
  //menor a 8%
  wifiManager.setMinimumSignalQuality();
  
  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  //wifiManager.setTimeout(120);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("Punto de Acceso REDCorp", "password")) {
    Serial.println("Fallo de Coneccion y Tiempo de Espera");
    delay(3000);
    //reiniciar y volver a intentarlo, o tal vez lo puso al sueño profundo
    ESP.reset();
    delay(5000);
  }

  //Si has llegado hasta aquí se ha conectado a la red WiFi
  Serial.println("Conexion Lista");

  //read updated parameters
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(blynk_token, custom_blynk_token.getValue());

  //save the custom parameters to FS
  if (shouldSaveConfig) {
    Serial.println("Guardando Configuracion");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject();
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["blynk_token"] = blynk_token;

    json["ip"] = WiFi.localIP().toString();
    json["gateway"] = WiFi.gatewayIP().toString();
    json["subnet"] = WiFi.subnetMask().toString();

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("Fallo al abrir el archivo de configuración para la escritura");
    }

    json.prettyPrintTo(Serial);
    json.printTo(configFile);
    configFile.close();
    //end save
  }

  Serial.println("IP Local");
  Serial.println(WiFi.localIP());
  Serial.println(WiFi.gatewayIP());
  Serial.println(WiFi.subnetMask());
  server.begin();
}

void loop() {

 // Lectur del Sensor de Temperatura
  tempInValue = analogRead(tempInPin);
  delay(200);
  celsius = tempInValue * 0.09765625;
  
  // escuchar a los clientes entrantes y solicitudes de proceso.
  checkForClient();
}

////////////////////////////////////////////////////////////////////////
//comprobar la función del cliente
////////////////////////////////////////////////////////////////////////
//
void checkForClient(){

  WiFiClient client = server.available();

  if (client) {

    // una petición http termina con una línea en blanco
    boolean currentLineIsBlank = true;
    boolean sentHeader = false;
    boolean login = false; //login

    while (client.connected()) {
      if (client.available()) {

        //Si la cabecera no se ha establecido enviarla
        
         //leer la entrada del usuario
        char c = client.read();
        
        readString.concat(c);  //login
         
          if(c == '*'){

          printHtmlHeader(client); //llamar para la cabecera HTML y CSS
          printLoginTitle(client);
          printHtmlFooter(client);
          //sentHeader = true;
          login=false;  //login 
          break;
        }
        
        if(!sentHeader){
          
            printHtmlHeader(client); //llamar para la cabecera HTML y CSS
            printHtmlButtonTitle(client); //imprimir el título del botón
            
          //Esto es para el ESP8266 para la construcción de la página sobre la marcha. 
          sentHeader = true;
        }

        // leer la entrada del usuario
        // char c = client.read();

        //si leyo, pero está en blanco no hubo lectura
        if(reading && c == ' '){
          reading = false;
          login=false;
        }

        //si hay un ? no había entrada de usuario
        if(c == '?') {
          reading = true; //encontrado el ?, comenzará la lectura de la información
        }
        //login if added
        if (login==false) {
            if(readString.indexOf("User=REDCORP&Pass=9876") > 0) { //Reemplace por su nombre de usuario y contraseña
              login=true;

            }
        }

       // cambie la entrada de la salida correspondiente
        if(login && reading){
          
          //Si la entrada del usuario es H establecer la salida a 1
          if(c == 'H') {
            outp = 1;
          }
          
          //Si la entrada del usuario es L establecer la salida a 0
          if(c == 'L') {
            outp = 0;
          }
          
         
          //Serial.print(outp);
          //Serial.print('\n');

          switch (c) {

             case '0':
               //añadir código aquí para disparar en 0
               triggerPin(outputAddress[0], client, outp);
               break;            
             case '1':
               //añadir código aquí para disparar en 1
               triggerPin(outputAddress[1], client, outp);
               break;             
             case '2':
               //añadir código aquí para disparar en 2
               triggerPin(outputAddress[2], client, outp);
               break;
             case '3':
               //añadir código aquí para disparar en 3
               triggerPin(outputAddress[3], client, outp);
               break;   
             case 'e':
               login=false;
               readString="";
               break; 
        
          } //Fin del switch case

        }//Fin del switch cambiando la salida relevante 

        //Si la entrada del usuario estaba en blanco
        if (c == '\n' && currentLineIsBlank){

          if(login == false)
          {
            printLoginTitle(client);
            printHtmlFooter(client);
            readString="";
          }
          
          printLastCommandOnce = true;
          printButtonMenuOnce = true;
          triggerPin(777, client, outp); //Llama para leer el menú de entrada y de impresión. 777 se utiliza no para actualizar las salidas
          break;
        }
        
      }
    }
    
    printHtmlFooter(client); //Imprime el pie de página html
 
  } 
else
   {  //Si no hay ningún cliente
  
      //Y hora la última página se sirve en más de un minuto.
      if (millis() > (timeConnectedAt + 3600000)){           

             if (writeToEeprom == true){ 
                 writeEepromValues();  //escribir en la EEPROM los estados de salida actuales
                 Serial.println("No hay clientes durante mas de Una Hora - Escribiendo estado en Eeprom.");
                 writeToEeprom = false;
             }
             
      }
   }


}


////////////////////////////////////////////////////////////////////////
//Funcion Disparo de pin
////////////////////////////////////////////////////////////////////////
//
void triggerPin(int pin, WiFiClient client, int outp){
  //Conexión o desconexión de las salidas, lee las salidas e imprime los botones   

  //Configuración de salidas
  if (pin != 777){ 

    if(outp == 1) {
      if (outputInverted ==false){ 
        digitalWrite(pin, HIGH);
      } 
      else{
        digitalWrite(pin, LOW);
      }
    }
    if(outp == 0){
      if (outputInverted ==false){ 
        digitalWrite(pin, LOW);
      } 
      else{
        digitalWrite(pin, HIGH);
      }
    }


  }
  //Actualizar la lectura de los resultados
  readOutputStatuses();


  //Imprime los botones
  if (printButtonMenuOnce == true){
    printHtmlButtons(client);
    printButtonMenuOnce = false;
  }

}

////////////////////////////////////////////////////////////////////////
//Funcion Imprimir Botones Html
////////////////////////////////////////////////////////////////////////
//imprimir los botones HTML para encender / apagar los canales
void printHtmlButtons(WiFiClient client){

  //Empezar a crear la tabla HTML
  client.println("");
  //client.println("<p>");
  client.println("<FORM>");
  client.println("<table border=\"0\" align=\"center\">");


  //Printing the Temperature
  client.print("<tr>\n");        

  client.print("<td><h4>");
  client.print("Temperatura ");
  client.print("</h4></td>\n");
  client.print("<td></td>");             
  client.print("<td>");
  client.print("<h3>");
  client.print(celsius);
  client.print(" &deg;C</h3></td>\n");
  
  client.print("<td></td>");
  client.print("</tr>");


  //Iniciar la impresión botón por botón
  for (int var = 0; var < outputQuantity; var++)  {      

    //conjunto de comandos para todos los on/off
    allOn += "H";
    allOn += outputAddress[var];
    allOff += "L";
    allOff += outputAddress[var];


    //Imprimir comienzo de la fila
    client.print("<tr>\n");        

    //Imprime el botón Texto
    client.print("<td><h4>");
    client.print(buttonText[var]);
    client.print("</h4></td>\n");

    //Imprime los botones ON
    client.print("<td>");
    //client.print(buttonText[var]);
    client.print("<INPUT TYPE=\"button\" VALUE=\"ON ");
    //client.print(buttonText[var]);
    client.print("\" onClick=\"parent.location='/?H");
    client.print(var);
    client.print("'\"></td>\n");

    //Imprime los botones OFF
    client.print(" <td><INPUT TYPE=\"button\" VALUE=\"OFF");
    //client.print(var);
    client.print("\" onClick=\"parent.location='/?L");
    client.print(var);
    client.print("'\"></td>\n");


    //Imprimir la primera parte de los círculos o los LEDs

    //Invertir los LEDs si se invierte la salida.

    if (outputStatus[var] == true ){                                                            //Si la salida está en ON
      if (outputInverted == false){                                                             //y si la salida no se invierte 
        client.print(" <td><div class='green-circle'><div class='glare'></div></div></td>\n"); //Imprimir HTML para LED ON
      }
      else{                                                                                    //las demas salidas se invierte a continuación,
        client.print(" <td><div class='black-circle'><div class='glare'></div></div></td>\n"); //Imprimir HTML para LED OFF
      }
    }
    else                                                                                      //Si la salida está en Off
    {
      if (outputInverted == false){                                                           //y si la salida no se invierte
        client.print(" <td><div class='black-circle'><div class='glare'></div></div></td>\n"); //Imprimir HTML para LED OFF
      }
      else{                                                                                   //las demas salidas se invierte a continuación 
        client.print(" <td><div class='green-circle'><div class='glare'></div></div></td>\n"); //Imprimir HTML para LED ON                  
      }
    }  




    //Imprimir final de la fila
    client.print("</tr>\n");  
  }

  //Mostrar u ocultar la impresión de todos los botónes
  if (switchOnAllPinsButton == true ){

    //Imprime ON en todos los Botnes
    client.print("<tr>\n<td><INPUT TYPE=\"button\" VALUE=\"Switch ON All Pins");
    client.print("\" onClick=\"parent.location='/?");
    client.print(allOn);
    client.print("'\"></td>\n");

    //Imprime OFF en todos los Botnes            
    client.print("<td><INPUT TYPE=\"button\" VALUE=\"Switch OFF All Pins");
    client.print("\" onClick=\"parent.location='/?");
    client.print(allOff);
    client.print("'\"></td>\n<td></td>\n<td></td>\n</tr>\n");
  }
   //LOGOUT
  //Cerrando table & form
  client.println("</table>");
  client.print("<h3 align=\"center\"><input type=button onClick=\"location.href='/?e'\" value='e'></h3>");  
  client.println("</FORM>");
  //client.println("</p>");  

}

////////////////////////////////////////////////////////////////////////
//Función leer los estados de salida 
////////////////////////////////////////////////////////////////////////
//lectura de los estados de salida
void readOutputStatuses(){
  for (int var = 0; var < outputQuantity; var++)  { 
    outputStatus[var] = digitalRead(outputAddress[var]);
    //Serial.print(outputStatus[var]);
  }

}

////////////////////////////////////////////////////////////////////////
//función leer los valores de la de Eeprom
////////////////////////////////////////////////////////////////////////
//Leer los valores EEPROM y guardar en outputStatus
void readEepromValues(){
    for (int adr = 0; adr < outputQuantity; adr++)  { 
    outputStatus[adr] = EEPROM.read(adr); 
    }
}

////////////////////////////////////////////////////////////////////////
//Funcion escribir valores en la eeprom
////////////////////////////////////////////////////////////////////////
//Escribir valores EEPROM
void writeEepromValues(){
    for (int adr = 0; adr < outputQuantity; adr++)  { 
    EEPROM.write(adr, outputStatus[adr]);
    }

} 

////////////////////////////////////////////////////////////////////////
//Funcion Iniciar valores de la eeprom
////////////////////////////////////////////////////////////////////////
//Inicializar valores de la eeprom
//si los valores de la EEPROM no son el formato correcto es decir, no es igual a 0 o 1 (por lo tanto mayor que 1) inicializar poniendo 0
void initEepromValues(){
      for (int adr = 0; adr < outputQuantity; adr++){        
         if (EEPROM.read(adr) > 1){
                EEPROM.write(adr, 0);
         } 
 
      }
  
}


////////////////////////////////////////////////////////////////////////
//Funcion Cabecera html
////////////////////////////////////////////////////////////////////////
//Imprime cabecera HTML
 void printHtmlHeader(WiFiClient client){
          
          timeConnectedAt = millis(); //Registra el tiempo cuando se sirve la última página.
          
          writeToEeprom = true; // Página Cargada por lo establecido en la acción de escritura en la EEPROM
          
          // enviar un encabezado de respuesta HTTP estándar
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connnection: close");
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<head>");

          // agregar título de la página 
          client.println("<title>Interruptores WiFi</title>");
          client.println("<meta name=\"description\" content=\"Interruptores WiFi\"/>");

          // añadir una etiqueta meta de actualización, por lo que el navegador tira de nuevo cada x segundos:
          client.print("<meta http-equiv=\"refresh\" content=\"");
          client.print(refreshPage);
          client.println("; url=/\">");

          // añadir otra configuración del navegador
          client.println("<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">");
          client.println("<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"default\">");
          client.println("<meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">");          

          //inserción de los datos de estilos, por lo general se encuentran en los archivos CSS
          client.println("<style type=\"text/css\">");
          client.println("");

          //Esto establecerá cómo se verá la página de forma gráfica
          client.println("html { height:100%; }");  

          client.println("  body {");
          client.println("    height: 100%;");
          client.println("    margin: 0;");
          client.println("    font-family: helvetica, sans-serif;");
          client.println("    -webkit-text-size-adjust: none;");
          client.println("   }");
          client.println("");
          client.println("body {");
          client.println("    -webkit-background-size: 100% 21px;");
          client.println("    background-color: #b1b2b2;");
          client.println("    background-image:");
          client.println("    -webkit-gradient(linear, left top, right top,");
          client.println("    color-stop(.75, transparent),");
          client.println("    color-stop(.75, rgba(255,255,255,.1)) );");
          client.println("    -webkit-background-size: 7px;");
          client.println("   }");
          client.println("");
          client.println(".view {");
          client.println("    min-height: 100%;");
          client.println("    overflow: auto;");
          client.println("   }");
          client.println("");
          client.println(".header-wrapper {");
          client.println("    height: 44px;");
          client.println("    font-weight: bold;");
          client.println("    text-shadow: rgba(0,0,0,0.7) 0 -1px 0;");
          client.println("    border-top: solid 1px rgba(255,255,255,0.6);");
          client.println("    border-bottom: solid 1px rgba(0,0,0,0.6);");
          client.println("    color: #fff;");
          client.println("    background-color: #2b74d2;");
          client.println("    background-image:");
          client.println("    -webkit-gradient(linear, left top, left bottom,");
          client.println("    from(rgba(255,255,255,.4)),");
          client.println("    to(rgba(255,255,255,.05)) ),");
          client.println("    -webkit-gradient(linear, left top, left bottom,");
          client.println("    from(transparent),");
          client.println("    to(rgba(0,0,64,.1)) );");
          client.println("    background-repeat: no-repeat;");
          client.println("    background-position: top left, bottom left;");
          client.println("    -webkit-background-size: 100% 21px, 100% 22px;");
          client.println("    -webkit-box-sizing: border-box;");
          client.println("   }");
          client.println("");
          client.println(".header-wrapper h1 {");
          client.println("    text-align: center;");
          client.println("    font-size: 20px;");
          client.println("    line-height: 44px;");
          client.println("    margin: 0;");
          client.println("   }");
          client.println("");
          client.println(".group-wrapper {");
          client.println("    margin: 9px;");
          client.println("    }");
          client.println("");
          client.println(".group-wrapper h2 {");
          client.println("    color: #4c566c;");
          client.println("    font-size: 17px;");
          client.println("    line-height: 0.8;");
          client.println("    font-weight: bold;");
          client.println("    text-shadow: #fff 0 1px 0;");
          client.println("    margin: 20px 10px 12px;");
          client.println("   }");
          client.println("");
          client.println(".group-wrapper h3 {");
          client.println("    color: #4c566c;");
          client.println("    font-size: 12px;");
          client.println("    line-height: 1;");
          client.println("    font-weight: bold;");
          client.println("    text-shadow: #fff 0 1px 0;");
          client.println("    margin: 20px 10px 12px;");
          client.println("   }");
          client.println("");
          client.println(".group-wrapper h4 {");  //Un texto descriptivo
          client.println("    color: #212121;");
          client.println("    font-size: 14px;");
          client.println("    line-height: 1;");
          client.println("    font-weight: bold;");
          client.println("    text-shadow: #aaa 1px 1px 3px;");
          client.println("    margin: 5px 5px 5px;");
          client.println("   }");
          client.println(""); 
          client.println(".group-wrapper table {");
          client.println("    background-color: #fff;");
          client.println("    -webkit-border-radius: 10px;");

          client.println("    -moz-border-radius: 10px;");
          client.println("    -khtml-border-radius: 10px;");
          client.println("    border-radius: 10px;");


          client.println("    font-size: 17px;");
          client.println("    line-height: 20px;");
          client.println("    margin: 9px 0 20px;");
          client.println("    border: solid 1px #a9abae;");
          client.println("    padding: 11px 3px 12px 3px;");
          client.println("    margin-left:auto;");
          client.println("    margin-right:auto;");

          client.println("    -moz-transform :scale(1);"); //Código para Mozilla Firefox
          client.println("    -moz-transform-origin: 0 0;");



          client.println("   }");
          client.println("");


          //cómo el LED (ON) verde se verá
          client.println(".green-circle {");
          client.println("    display: block;");
          client.println("    height: 23px;");
          client.println("    width: 23px;");
          client.println("    background-color: #0000ff;");
          //client.println("    background-color: rgba(60, 132, 198, 0.8);");
          client.println("    -moz-border-radius: 11px;");
          client.println("    -webkit-border-radius: 11px;");
          client.println("    -khtml-border-radius: 11px;");
          client.println("    border-radius: 11px;");
          client.println("    margin-left: 1px;");

          client.println("    background-image: -webkit-gradient(linear, 0% 0%, 0% 90%, from(rgba(0,26,184,0.80)), to(rgba(112,159,255,0.90)));@");
          client.println("    border: 2px solid #ccc;");
          client.println("    -webkit-box-shadow: rgba(11,17,140,0.50) 0px 10px 16px;");
          client.println("    -moz-box-shadow: rgba(11,17,140,0.50) 0px 10px 16px; /* FF 3.5+ */");
          client.println("    box-shadow: rgba(11,17,140,0.50) 0px 10px 16px; /* FF 3.5+ */");

          client.println("    }");
          client.println("");

          //cómo el LED (apagado) se verá negro
          client.println(".black-circle {");
          client.println("    display: block;");
          client.println("    height: 23px;");
          client.println("    width: 23px;");
          client.println("    background-color: #001044;");
          client.println("    -moz-border-radius: 11px;");
          client.println("    -webkit-border-radius: 11px;");
          client.println("    -khtml-border-radius: 11px;");
          client.println("    border-radius: 11px;");
          client.println("    margin-left: 1px;");
          client.println("    -webkit-box-shadow: rgba(11,17,140,0.50) 0px 10px 16px;");
          client.println("    -moz-box-shadow: rgba(11,17,140,0.50) 0px 10px 16px; /* FF 3.5+ */"); 
          client.println("    box-shadow: rgba(11,17,140,0.50) 0px 10px 16px; /* FF 3.5+ */");
          client.println("    }");
          client.println("");

          //añadir el resplandor en ambos LEDs
          client.println("   .glare {");
          client.println("      position: relative;");
          client.println("      top: 1;");
          client.println("      left: 5px;");
          client.println("      -webkit-border-radius: 10px;");
          client.println("      -moz-border-radius: 10px;");
          client.println("      -khtml-border-radius: 10px;");
          client.println("      border-radius: 10px;");
          client.println("      height: 1px;");
          client.println("      width: 13px;");
          client.println("      padding: 5px 0;");
          client.println("      background-color: rgba(200, 200, 200, 0.25);");
          client.println("      background-image: -webkit-gradient(linear, 0% 0%, 0% 95%, from(rgba(255, 255, 255, 0.7)), to(rgba(255, 255, 255, 0)));");
          client.println("    }");
          client.println("");


          //finalmente, este es el final de los datos de estilo y cabecera
          client.println("</style>");
          client.println("</head>");

          //Ahora la impresión de la página en sí
          client.println("<body>");
          client.println("<div class=\"view\">");
          client.println("    <div class=\"header-wrapper\">");
          client.println("      <h1>Interruptores WiFi</h1>");
          client.println("    </div>");

//////

 } //final de la cabecera HTML

////////////////////////////////////////////////////////////////////////
//Función de pie de página html
////////////////////////////////////////////////////////////////////////
//impresion de pie de página html
void printHtmlFooter(WiFiClient client){
    //Establecer las variables antes de salir 
    printLastCommandOnce = false;
    printButtonMenuOnce = false;
    allOn = "";
    allOff = "";
    
    //imprimir última parte del html
    client.println("\n<h3 align=\"center\"> Autor - REDCorp.tk &copy; <br> Chihuahua,Mexico - Mayo - 2016 - REV ");
    client.println(rev);
    client.println("<div align=\"center\"><a href=\"http://www.REDCorp.tk\" target=\"_blank\">REDCorp.tk &copy; </a><br></div>");
    client.println("</h3></div>\n</div>\n</body>\n</html>");

    delay(1); // dar tiempo al navegador web para recibir los datos

    client.stop(); // Cerrar la conexion:
    
    
    delay (2); //Espera para que se le de tiempo a la memoria intermedia del cliente para limpiar y que no se repitan varias páginas.
    
 } //final de pie de página html


////////////////////////////////////////////////////////////////////////
//Funcion Imprimir boton HTML del Titulo
////////////////////////////////////////////////////////////////////////
//Imprimir boton HTML del titulo
void printHtmlButtonTitle(WiFiClient client){
          client.println("<div  class=\"group-wrapper\">");
          client.println("    <h2>Estado de las Salidas.</h2>");
          client.println();
}


////////////////////////////////////////////////////////////////////////
//Funcion printLoginTitle
////////////////////////////////////////////////////////////////////////
//Las impresiones de título botón html
void printLoginTitle(WiFiClient client){
      client.println("<div  class=\"group-wrapper\">");
  client.println("<h2 align=\"center\">Bienvenido...</h2>");
  client.println("<h2 align=\"center\">Introduzca Usuario y Contrasena.</h2>");
  client.println(" </div>");
  client.print("<form action='192.168.1.102/'>"); //change to your IP
  client.print("");
  client.println("    <div class=\"group-wrapper\">");
  client.print("<h2 align=\"center\">Usuario:</h2>");
  client.print("<h2 align=\"center\"><input name='User' value=''></h2>");
  client.print("<h2 align=\"center\">Contrasena:</h2>");
  client.print("<h2 align=\"center\"><input type='Password' name='Pass' value=''></h2>");
  client.print("</div>");
  client.print("<h2 align=\"center\"><input type='submit' value=' Entrar '></h2>");
  //client.print("<input type='submit' value=' Login '>");
  client.print("<hr /></form><hr />");
  client.println("</head></center>");
}
