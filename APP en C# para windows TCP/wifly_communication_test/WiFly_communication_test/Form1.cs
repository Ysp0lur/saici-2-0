﻿//TCPClient and TCPListener test program
//by: Julian Y. Cheng (UCDavis)   May 2012
//DISCLAIMER: for educational use only
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace WiFly_communication_test
{
    public partial class Form1 : Form
    {
        /*TCPClient variable created here to be thrown around in different threads & funcs*/
        private TcpClient myClient;
        private NetworkStream stream;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            /*Get the byte array of the message and send it to the stream*/
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(txtSend.Text);

            stream.Write(data, 0, data.Length);
            txtTalk.AppendText("Sent message: " + txtSend.Text + "\r\n");
            if (checkBox1.Checked) txtSend.Clear(); //clears entry field if checkbox is checked
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            /*Parse IP & port info and connect to client, the stream has global scope so that other funcs can use
              Note the usage of BGW is just for realtime display of received messages*/
            myClient = new TcpClient(textBox1.Text,int.Parse(textBox2.Text));

            stream = myClient.GetStream();
            txtTalk.AppendText("Connected to: " + textBox1.Text + ":" + textBox2.Text + "\r\n");
            backgroundWorker1.RunWorkerAsync();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            /*Disconnect from client, close stream first then TCP connection
             NOTE: this is the Disconnect button, not to be confused with server stop(below)*/
            txtTalk.AppendText("Disconnected\r\n");
            stream.Close();
            myClient.Close();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            /*Listen for incoming messages indefinitely until connection is broke*/
            while (myClient.Connected)
            {
                if (!stream.DataAvailable)
                {
                    System.Threading.Thread.Sleep(250);
                    continue;
                }

                Byte[] data = new Byte[256];

                String responseData = String.Empty;

                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                backgroundWorker1.ReportProgress(0, responseData);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtTalk.AppendText("Received: " + e.UserState.ToString() + "\r\n");
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            txtTalk.AppendText("Background worker stopped\r\n");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtTalk.Clear();
        }
        
        /*Server variables have global scope for simplicity*/
        private TcpListener tcpListener;
        private Thread listenThread;

        private void btnStart_Click(object sender, EventArgs e)
        {
            /*Start listener and kick to new thread to free the UI*/
            tcpListener = new TcpListener(IPAddress.Any, int.Parse(textBox2.Text));
            listenThread = new Thread(new ThreadStart(ListenForClients));
            listenThread.Start();
            this.Invoke((MethodInvoker)delegate
            {
                //This unnamed delegate is used to access UI elements on the main thread
                txtTalk.AppendText("Server started\r\nListening for clients...\r\n");
            });
        }

        private void ListenForClients()
        {
            /*Listner thread for connections, runs indefinitely until server stopped
             kicks incomming connection communication to new thread*/
            tcpListener.Start();

            try
            {
                while (true)
                {
                    TcpClient client = tcpListener.AcceptTcpClient();   //blocking call

                    Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
                    clientThread.Start(client);
                    this.Invoke((MethodInvoker)delegate
                    {
                        txtTalk.AppendText("Client connected\r\n");
                    });
                }
            }
            catch (SocketException)
            {
                //This exception is thrown when server is terminated
                //caught just so that the program doesn't crash
            }
            finally
            {
                this.Invoke((MethodInvoker)delegate
                {
                    txtTalk.AppendText("Server stopped\r\n");
                });
            }
        }

        private void HandleClientComm(object client)
        {
            /*Handles all communication once server receives client*/
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();

            byte[] message = new byte[4096];
            int bytesRead;

            while (true)
            {
                bytesRead = 0;

                try
                {
                    bytesRead = clientStream.Read(message, 0, message.Length);  //blocking call
                }
                catch (Exception ex)
                {
                    txtTalk.AppendText("ERROR: " + ex.Message + "\r\n");
                    break;
                }

                if (bytesRead == 0)
                {
                    break;
                }

                ASCIIEncoding encoder = new ASCIIEncoding();
                this.Invoke((MethodInvoker)delegate
                {
                    txtTalk.AppendText("Recieved: " + encoder.GetString(message, 0, bytesRead) + "\r\n");
                });
            }

            tcpClient.Close();
            this.Invoke((MethodInvoker)delegate
            {
                txtTalk.AppendText("Client disconnected\r\n");
            });
        }

        private void btnStopServ_Click(object sender, EventArgs e)
        {
            /*Stops the server
             Note that because stop() will cause the listener to terminate with an exception (see above)
             any code after this statement in this event handler will be skipped*/
            tcpListener.Stop();
            /*DON'T PUT ANYTHING HERE, NEVER EXECUTES*/
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            /*Get the byte array of the message and send it to the stream*/
            Byte[] data = System.Text.Encoding.ASCII.GetBytes(txtSend.Text);

            stream.Write(data, 0, data.Length);
            txtTalk.AppendText("Sent message: " + txtSend.Text + "\r\n");
            if (checkBox1.Checked) txtSend.Clear(); //clears entry field if checkbox is checked
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            /*Parse IP & port info and connect to client, the stream has global scope so that other funcs can use
             Note the usage of BGW is just for realtime display of received messages*/
            myClient = new TcpClient(textBox1.Text, int.Parse(textBox2.Text));

            stream = myClient.GetStream();
            txtTalk.AppendText("Connected to: " + textBox1.Text + ":" + textBox2.Text + "\r\n");
            backgroundWorker1.RunWorkerAsync();
        }

        private void toolStripButtonDesconectar_Click(object sender, EventArgs e)
        {
            /*Disconnect from client, close stream first then TCP connection
             NOTE: this is the Disconnect button, not to be confused with server stop(below)*/
            txtTalk.AppendText("Disconnected\r\n");
            stream.Close();
            myClient.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            webBrowser1.Navigate("192.168.0.200");
        }
    }
}
